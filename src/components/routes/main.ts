import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-home-route": index.HomeRouteComponent;
    }
}

customElements.define("app-home-route", index.HomeRouteComponent);

