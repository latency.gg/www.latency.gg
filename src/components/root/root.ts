import { html, nothing } from "lit";
import { state } from "lit/decorators.js";
import * as application from "../../application/index.js";
import { ComponentBase } from "../base.js";

export class RootComponent extends ComponentBase {
    static styles = [];

    @state()
    routeElement?: HTMLElement = this.getRouteElement();

    getRouteElement() {
        const info = application.getRoute();
        if (info == null) return;

        const element = this.ownerDocument.createElement("app-" + info.name + "-route");
        Object.entries(info.parameters).
            forEach(([key, value]) => element.setAttribute(key, value));

        return element;
    }

    render() {
        const { routeElement } = this;

        if (!window.applicationContext) {
            return nothing;
        }

        return html`
<app-layout>
    ${routeElement}
</app-layout>
`;
    }

    connectedCallback() {
        super.connectedCallback();

        window.addEventListener("popstate", this.handlePopState);
        window.addEventListener("navigate", this.handleNavigate);
    }

    disconnectedCallback() {
        super.disconnectedCallback();

        window.removeEventListener("popstate", this.handlePopState);
        window.removeEventListener("navigate", this.handleNavigate);
    }

    private handlePopState = () => {
        this.routeElement = this.getRouteElement();
    }

    private handleNavigate = () => {
        this.routeElement = this.getRouteElement();
    }
}
