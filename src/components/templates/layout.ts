import { css, html } from "lit";
import { ComponentBase } from "../base.js";

export class LayoutComponent extends ComponentBase {

    static styles = [...super.styles, css`
main {
    width: 400px;
    margin-left: auto;
    margin-right: auto;
    padding-bottom: 4em;
}
`];

    render() {
        return html`
<main>
    <slot></slot>
</main>
`;

    }

}
