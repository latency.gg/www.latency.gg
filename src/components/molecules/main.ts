import * as index from "./index.js";

declare global {
    interface HTMLElementTagNameMap {
        "app-logo": index.LogoComponent;
    }
}

customElements.define("app-logo", index.LogoComponent);

