import assert from "assert";
import * as http from "http";
import Koa from "koa";
import koaStatic from "koa-static";
import * as path from "path";
import * as selenium from "selenium-webdriver";
import * as seleniumChrome from "selenium-webdriver/chrome.js";
import * as seleniumEdge from "selenium-webdriver/edge.js";
import * as seleniumFirefox from "selenium-webdriver/firefox.js";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";
import { projectRoot } from "../utils/root.js";

export type ContextBrowser =
    "firefox" | "firefox-headless" |
    "chrome" | "chrome-headless" |
    // safari does not have a headless mode (https://discussions.apple.com/thread/251837694)
    "safari" |
    "edge" | "edge-headless";
export const contextBrowser = process.env.BROWSER as ContextBrowser || undefined;

interface Context {
    endpoints: {
        frontend: URL,
    },
    servers: {
        frontend: Koa,
    },
    createBrowser(): Promise<selenium.WebDriver>,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
    onError?: (error: unknown) => void,
) {
    const endpoints = {
        frontend: new URL("http://localhost:8080"),
    };

    const applicationSettings: application.Settings = {
    };

    const applicationContext = application.createContext(
        applicationSettings,
    );

    const servers = {
        frontend: new Koa(),
    };

    const mainHtml = application.renderMainHtml(applicationSettings);

    servers.frontend.use(koaStatic(path.join(projectRoot, "assets")));
    servers.frontend.use(koaStatic(path.join(projectRoot, "bundle")));
    servers.frontend.use((context, next) => {
        const route = application.router.parseRoute(context.path);
        if (!route) return next();

        context.body = mainHtml;
    });

    const httpServers = {
        frontend: http.createServer(servers.frontend.callback()),
    };

    const keys = Object.keys(endpoints) as Array<keyof typeof endpoints>;
    await Promise.all(
        keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
            endpoints[key].port,
            () => resolve(),
        ))),
    );
    try {
        const destructors = new Array<() => Promise<void>>();
        try {
            const context = {
                endpoints,
                servers,
                async createBrowser() {
                    assert(contextBrowser, "context browser not set");

                    let builder = new selenium.Builder();
                    switch (contextBrowser) {
                        case "firefox":
                        case "chrome":
                        case "safari":
                        case "edge":
                            builder = builder.forBrowser(contextBrowser);
                            break;

                        case "firefox-headless":
                            builder = builder.forBrowser("firefox");
                            builder = builder.setFirefoxOptions(
                                new seleniumFirefox.Options().headless(),
                            );
                            break;

                        case "chrome-headless":
                            builder = builder.forBrowser("chrome");
                            builder = builder.setChromeOptions(
                                new seleniumChrome.Options().headless(),
                            );
                            break;

                        case "edge-headless":
                            builder = builder.forBrowser("edge");
                            builder = builder.setEdgeOptions(
                                new seleniumEdge.Options().headless(),
                            );
                            break;

                        default: assert.fail(`unknown context browser ${contextBrowser}`);
                    }

                    const driver = await builder.build();
                    destructors.push(() => driver.quit());

                    return driver;
                },
            };

            await job(context);
        }
        finally {
            await Promise.all(destructors.map(destructor => destructor()));
        }
    }
    finally {
        await Promise.all(keys.map(
            async key => new Promise<void>((resolve, reject) => httpServers[key].close(
                error => error ?
                    reject(error) :
                    resolve(),
            )),
        ));
    }
}
