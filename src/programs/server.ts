import { program } from "commander";
import * as http from "http";
import Koa from "koa";
import koaStatic from "koa-static";
import * as path from "path";
import * as application from "../application/index.js";
import { projectRoot } from "../utils/root.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    option("--self-endpoint <url>", "Endpoint of this service", v => new URL(v), new URL("http://localhost:8080/")).
    action(action);

interface ActionSettings {
    port: number;
    selfEndpoint: URL;
}
async function action({
    port,
    selfEndpoint,
}: ActionSettings) {
    console.log("starting...");

    const applicationSettings: application.Settings = {
    };

    const server = new Koa();

    const mainHtml = application.renderMainHtml(applicationSettings);

    server.use(koaStatic(path.join(projectRoot, "assets")));
    server.use(koaStatic(path.join(projectRoot, "bundle")));
    server.use((context, next) => {
        const route = application.router.parseRoute(context.path);
        if (!route) return next();

        context.body = mainHtml;
    });

    const httpServer = http.createServer(server.callback());

    await new Promise<void>(resolve => httpServer.listen(
        port,
        () => resolve(),
    ));
    try {
        console.log("started");

        await new Promise(resolve => process.addListener("SIGINT", resolve));

        console.log("stopping...");
    }
    finally {
        await new Promise<void>((resolve, reject) => httpServer.close(
            error => error ?
                reject(error) :
                resolve(),
        ));
    }

    console.log("stopped");
}
